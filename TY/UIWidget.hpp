#pragma once

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Rect.hpp>
#include "IUpdateable.hpp"

class Game;

class UIWidget :
    public sf::Drawable,
    public IUpdateable
{
public:
    UIWidget( Game *_pGame );
    ~UIWidget();

    void SetRect( sf::FloatRect const& _rect );
    void SetPosition( float _x, float _y );
    void SetSize( float _width, float _height );

    sf::FloatRect GetRect() const { return m_rect; }
    sf::Vector2f GetPosition() const { return sf::Vector2f( m_rect.left, m_rect.top ); }
    sf::Vector2f GetSize() const { return sf::Vector2f( m_rect.width, m_rect.height ); }

protected:

    virtual void OnResized() = 0;

    sf::FloatRect m_rect;

    Game *m_pGame;
};

