#include "SceneManager.hpp"

#include "Game.hpp"
#include "Updater.hpp"
#include "BannerSequenceScene.hpp"
#include "MainMenuScene.hpp"
#include "GameplayScene.hpp"
#include "TetrisScene.hpp"

SceneManager::SceneManager( Game *_pGame ) :
    m_currScene( nullptr ),
    m_pGame( _pGame )
{
}

SceneManager::~SceneManager()
{
}

bool SceneManager::Init( Game* _pGame )
{
    bool isInitOk = true;

    m_pGame = _pGame;

    return isInitOk;
}

void SceneManager::ShutDown()
{
    BeginScene( -1 );
}

void SceneManager::BeginFirstScene()
{
    //BeginScene( Scene_IF::SceneID_BannerSequence );
    BeginScene( Scene_IF::SceneID_MainMenu );
}

void SceneManager::BeginScene( int _sceneID )
{
    if ( m_currScene )
    {
        m_currScene->End();

        delete m_currScene;
        m_currScene = nullptr;
    }

    switch ( _sceneID )
    {
    case Scene_IF::SceneID_BannerSequence:
        m_currScene = new BannerSequenceScene( this, m_pGame );
        break;

    case Scene_IF::SceneID_MainMenu:
        m_currScene = new MainMenuScene( this, m_pGame );
        break;

    case Scene_IF::SceneID_Gameplay:
        m_currScene = new GameplayScene( this, m_pGame );
        break;

    case Scene_IF::SceneID_Tetris:
        m_currScene = new TetrisScene( this, m_pGame );
        break;
    }

    if ( m_currScene )
    {
        m_currScene->Begin();
    }
}
