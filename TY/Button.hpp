#pragma once

#include "UIWidget.hpp"
#include <string>

class ButtonState;

namespace sf
{
    class RectangleShape;
    class Font;
    class Text;
}

typedef void (*btnCallbackFn)( Game *_pGame, void *_data );

class Button :
    public UIWidget
{
    typedef UIWidget SUPER;

    friend class ButtonStateNotHovered;
    friend class ButtonStateHovered;
    friend class ButtonStateClicked;
    friend class ButtonStateReleased;

public:
    Button( Game* _pGame );
    ~Button();

    void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;

    void Update( UpdateStats const& _stats ) override;

    void SetText( std::string const & _text );

    void SetCallbackFn( btnCallbackFn _callback, void *_pData = nullptr );

private:

    void OnResized() override;

    sf::RectangleShape *m_rect;
    sf::Font *m_font;
    sf::Text *m_text;

    ButtonState *m_currState;
    void SetCurrState( ButtonState *_state );

    void *m_pData;
    btnCallbackFn m_pCallback;
    void CallCallbackFn();
};

