#include "Slider.hpp"
#include "Game.hpp"
#include "SliderState.hpp"

#include <SFML/Graphics.hpp>

#include <iostream>
#include <cmath>

Slider::Slider( Game* _pGame ) :
    SUPER( _pGame ),
    m_currState( nullptr ),
    m_minVal( 0.0f ),
    m_maxVal( 0.0f ),
    m_range( 0.0f ),
    m_currVal( 0.0f ),

    m_background( new sf::RectangleShape() ),
    m_slider( new sf::RectangleShape() )
{
    SetCurrState( &SliderState::NotHovered );

    m_background->setFillColor( sf::Color::Green );
}

Slider::~Slider()
{
    delete m_background;
    delete m_slider;
}

void Slider::Update( UpdateStats const& _stats )
{
    m_currState->Update( this, m_pGame->GetInput() );
    SetVal( GetMin() + (GetRange() * GetVisualFillToPercentage()) );
}

void Slider::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    target.draw( *m_background );
    target.draw( *m_slider );
}

void Slider::SetRange( float _min, float _max )
{
    if( _min >= _max )
    {
        std::cout << __FILE__ << __LINE__
            << "Max value can not be smaller than min!" << std::endl;

        m_minVal = 0.0f;
        m_maxVal = 0.0f;
        m_range = 0.0f;
    }
    else
    {
        m_minVal = _min;
        m_maxVal = _max;
        m_range = fabs( m_maxVal - m_minVal );
        m_currVal = m_minVal;

        OnResized();
    }
}

void Slider::SetVal( float _val )
{
    m_currVal = _val;

    if( m_currVal < m_minVal ) m_currVal = m_minVal;
    if( m_currVal > m_maxVal ) m_currVal = m_maxVal;
}

void Slider::AddVal( float _delta )
{
#if 0
    m_currVal += _delta;

    if( m_currVal < m_minVal ) m_currVal = m_minVal;
    if( m_currVal > m_maxVal ) m_currVal = m_maxVal;
#else
    m_currVal -= _delta;

    if( m_currVal <= 0.0f ) m_currVal = 0.0f;
    if( m_currVal >= m_background->getSize().x - m_slider->getSize().x )
        m_currVal = m_background->getSize().x - m_slider->getSize().x;
#endif
}

float Slider::GetValueInPercentage()
{
    return  ( 100.0f * (m_currVal - m_minVal) ) / GetRange();
}

float Slider::GetVisualFillToPercentage()
{
    return m_slider->getSize().x / GetSize().x;
}

void Slider::OnResized()
{
    sf::Vector2f const pos = GetPosition();
    sf::Vector2f const size = GetSize();

    m_background->setPosition( pos );
    m_background->setSize( size );

    m_slider->setPosition( pos );
    m_slider->setSize( sf::Vector2f( size.x * GetValueInPercentage(), size.y ) );
}

void Slider::SetCurrState( SliderState* _state )
{
    if (m_currState) m_currState->OnLeave(this);

    m_currState = _state;
    m_currState->OnEnter( this, m_pGame->GetInput() );
}
