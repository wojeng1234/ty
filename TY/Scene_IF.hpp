#pragma once

class Scene_IF
{
public:
    Scene_IF();
    virtual ~Scene_IF();

    void Begin() { OnBegin(); }
    void End() { OnEnd(); }

    enum
    {
        SceneID_BannerSequence,
        SceneID_MainMenu,
        SceneID_Gameplay,
        SceneID_Tetris
    };

    virtual int GetSceneID() = 0;

protected:

    virtual void OnBegin() = 0;
    virtual void OnEnd() = 0;

};

