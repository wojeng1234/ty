#pragma once

#include <list>
#include <SFML/System/Vector2.hpp>

namespace sf
{
    class Drawable;
    class RenderWindow;
    class Color;
}

class GUIRenderer
{
public:
    GUIRenderer(sf::RenderWindow* _renderWindow);
    ~GUIRenderer();

    void Render() const;

    void AddDrawable(sf::Drawable* _drawable);
    void RemoveDrawable(sf::Drawable* _drawable);
    void ClearAllDrawables();

    sf::Vector2u GetWindowSize();

private:
    GUIRenderer() = delete;

    //TODO: Add custom structure instead of sf::Drawable
    std::list<sf::Drawable*> m_drawables;

    sf::RenderWindow* const m_renderWindow;

};

