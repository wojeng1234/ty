#include "GameplayScene.hpp"
#include "Game.hpp"
#include "Input.hpp"
#include "SceneRenderer.hpp"
#include "Scene2DCamera.hpp"

#include <SFML/Graphics.hpp>
#include <cmath>

GameplayScene::GameplayScene( SceneManager* _pSceneManager, Game* _pGame ) :
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_pSceneManager( _pSceneManager )
{
    m_rect = new sf::RectangleShape();
    m_texture = new sf::Texture();
}

GameplayScene::~GameplayScene()
{
    delete m_rect;
    delete m_texture;
}

void GameplayScene::Update( UpdateStats const& _stats )
{
    sf::Vector2f const mp = m_pGame->GetInput()->GetMouseScenePos();

#if 1
    int x = ((int)_stats.m_totalTime % 2) * 512;
    int y = ((int)_stats.m_totalTime / 2) % 2 * 512;
    m_rect->setTextureRect( sf::IntRect(x, y, 512, 512) );
#else
    if( m_rect->getGlobalBounds().contains( mp ) )
    {
        m_rect->setFillColor( sf::Color::Red );
    }
    else
    {
        m_rect->setFillColor( sf::Color::Blue );
    }
#endif

    m_pGame->GetSceneRenderer()->GetCamera()->SetCenter( sf::Vector2f(sin(_stats.m_totalTime), cos(_stats.m_totalTime)) * 20.0f );

    //printf( "X: %d,\tY: %d\n", (int)mp.x, (int)mp.y );
}

void GameplayScene::OnBegin()
{
    puts("Hello GameplayScene!");

    sf::Vector2f const wantedSize = sf::Vector2f( 400.0f, 400.0f );

    std::string const bannerName = "files/pictures/WojtekAndWojciech.png";
    m_texture->loadFromFile( bannerName );

    m_rect->setTexture( m_texture );

    m_rect->setSize( sf::Vector2f(500.0f, 500.0f) );
    m_rect->setOrigin( m_rect->getSize() * 0.5f );

    m_pGame->GetSceneRenderer()->AddDrawable( m_rect );
}

void GameplayScene::OnEnd()
{
}
