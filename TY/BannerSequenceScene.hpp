#pragma once

#include "Scene_IF.hpp"
#include "IUpdateable.hpp"

namespace sf
{
    class Texture;
    class RectangleShape;
    class Font;
    class Text;
}

class Game;
class SceneManager;

class BannerSequenceScene :
    public Scene_IF,
    public IUpdateable
{
public:
    BannerSequenceScene( SceneManager *_pSceneManager, Game *_pGame );
    ~BannerSequenceScene();

    void Update( UpdateStats const& _stats ) override;

    int GetSceneID() override { return SceneID_BannerSequence; }

private:

    void OnBegin() override;
    void OnEnd() override;

    Game *m_pGame;
    SceneManager *m_pSceneManager;

    float const m_totalDisplayTime;
    float m_timeElapsed;

    sf::Texture *m_texture;
    sf::RectangleShape *m_sprite;

    sf::Font *m_bannerLabelFont;
    sf::Text *m_label1;
    sf::Text *m_label2;
};

