#include "UIWidget.hpp"

#include "Game.hpp"

UIWidget::UIWidget( Game* _pGame ) :
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_rect( 0.0f, 0.0f, 0.0f, 0.0f )
{
}

UIWidget::~UIWidget()
{
}

void UIWidget::SetRect( sf::FloatRect const& _rect )
{
    m_rect = _rect;

    OnResized();
}

void UIWidget::SetPosition( float _x, float _y )
{
    m_rect.left = _x;
    m_rect.top = _y;

    OnResized();
}

void UIWidget::SetSize( float _width, float _height )
{
    m_rect.width = _width;
    m_rect.height = _height;

    OnResized();
}
