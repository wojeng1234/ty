#include "Updater.hpp"
#include "IUpdateable.hpp"
#include "UpdateStats.hpp"

#include <memory>
#include <string.h>

int const MAX_SIZE = 100;

Updater::Updater() :
    m_numUpdates( 0 ),
    m_updateables( new IUpdateable*[MAX_SIZE] )
{
    memset( m_updateables, 0, MAX_SIZE * sizeof(IUpdateable*) );
}

Updater::~Updater()
{
    delete[] m_updateables;
}

void Updater::Register( IUpdateable* _item )
{
    if( m_numUpdates < MAX_SIZE )
    {
        m_updateables[m_numUpdates] = _item;
        m_numUpdates++;
    }
    else
    {
        //Handle
    }
}

void Updater::Unregister( IUpdateable* _item )
{
    int index = -1;
    for( int i = 0; i < m_numUpdates; ++i )
    {
        if( m_updateables[i] == _item )
        {
            index = i;
            break;
        }
    }

    if( index >= 0 )
    {
        m_updateables[index] = nullptr;

        m_numUpdates--;
        m_updateables[index] = m_updateables[m_numUpdates];

        m_updateables[m_numUpdates] = nullptr;
    }
    else
    {
        //Handle
    }
}

void Updater::Update( float _deltaTime, float _totalTime )
{
    UpdateStats const updateStats = UpdateStats( _deltaTime, _totalTime );
    int const numToUpdate = m_numUpdates;

    for( int i = 0; i < numToUpdate; ++i )
    {
        if( m_updateables[i] )
        {
            m_updateables[i]->Update( updateStats );
        }
    }
}