#pragma once
#include <list>

class SceneManager;
class SceneRenderer;
class GUIRenderer;
class Updater;
class Input;

namespace sf
{
    class Clock;
    class RenderWindow;
    class Color;
}

class Game
{
public:
    Game();
    ~Game();

    bool Init();
    void Start();
    void ShutDown();

    void DoMainLoop();

    inline SceneManager* GetSceneManager() { return m_sceneManager; }
    inline SceneRenderer* GetSceneRenderer() { return m_sceneRenderer; }
    inline GUIRenderer* GetGUIRenderer() { return m_GUIRenderer; }
    inline Updater* GetUpdater() { return m_pUpdater; }
    inline Input* GetInput() { return m_input; }

    void SetWndClearColor( sf::Color const &_color );

    void ExitGame();

private:
    sf::RenderWindow *const m_window;

    sf::Color *m_wndClearColor;

    sf::Clock *const m_clock;
    float m_totalTile;
    float m_deltaTime;

    SceneManager* const m_sceneManager;
    SceneRenderer *const m_sceneRenderer;
    GUIRenderer *const m_GUIRenderer;
    Updater *const m_pUpdater;
    Input *const m_input;

    void CloseWindow();
};
