#pragma once

#include "Scene_IF.hpp"
#include "IUpdateable.hpp"

class Game;
class SceneManager;
class Button;
class TetrisBackground;

class TetrisScene :
    public Scene_IF,
    public IUpdateable
{
public:
    TetrisScene( SceneManager* _pSceneManager, Game* _pGame );
    ~TetrisScene();

    void Update( UpdateStats const& _stats ) override;

    int GetSceneID() override { return SceneID_Tetris; }

private:

    void OnBegin() override;
    void OnEnd() override;

    Game* m_pGame;
    SceneManager* m_pSceneManager;

    Button *m_backButton;

    TetrisBackground *m_tetrisBackground;

    float m_brickFallIntervall;
};
