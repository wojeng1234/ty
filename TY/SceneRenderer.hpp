#pragma once

#include <list>

class Scene2DCamera;

namespace sf
{
    class Drawable;
    class RenderWindow;
    class Color;
    class Shader;
}

class SceneRenderer
{
public:
    SceneRenderer( sf::RenderWindow *_renderWindow );
    ~SceneRenderer();

    void Render() const;

    void AddDrawable( sf::Drawable *_drawable );
    void RemoveDrawable( sf::Drawable *_drawable );
    void ClearAllDrawables();

    Scene2DCamera *GetCamera() { return m_camera; }

private:
    SceneRenderer() = delete;

    //TODO: Add custom structure instead of sf::Drawable
    std::list<sf::Drawable*> m_drawables;

    sf::RenderWindow *const m_renderWindow;

    sf::Shader *const m_shader;

    Scene2DCamera *m_camera;

};

