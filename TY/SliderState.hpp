#pragma once

class Slider;
class Input;

class SliderStateNotHovered;
class SliderStateHovered;
class SliderStatePressed;
class SliderStateReleased;

class SliderState
{
public:
    SliderState() {}
    virtual ~SliderState() {}

    virtual void Update(Slider* _slider, Input* _input ) = 0;
    virtual void OnEnter( Slider* _slider, Input* _input ) {};
    virtual void OnLeave( Slider* _slider ) {};

    static SliderStateNotHovered NotHovered;
    static SliderStateHovered Hovered;
    static SliderStatePressed Pressed;
    static SliderStateReleased Released;
};

////////////////////////////////////////////////////
class SliderStateNotHovered :
    public SliderState
{
public:
    void Update( Slider* _slider, Input* _input ) override;
    void OnEnter( Slider *_slider, Input* _input ) override;
};

////////////////////////////////////////////////////
class SliderStateHovered :
    public SliderState
{
public:
    void Update( Slider* _slider, Input* _input ) override;
    void OnEnter( Slider *_slider, Input* _input ) override;
};

////////////////////////////////////////////////////
class SliderStatePressed :
    public SliderState
{
public:
    void Update( Slider* _slider, Input* _input ) override;
    void OnEnter( Slider *_slider, Input* _input ) override;

private:
    float m_press_offset{};
};

////////////////////////////////////////////////////
class SliderStateReleased :
    public SliderState
{
public:
    void Update( Slider* _slider, Input* _input ) override;
};
