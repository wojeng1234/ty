#include "TetrisScene.hpp"
#include "Updater.hpp"
#include "Game.hpp"
#include "SceneManager.hpp"
#include "SceneRenderer.hpp"
#include "GUIRenderer.hpp"
#include "Scene2DCamera.hpp"
#include "Button.hpp"
#include "TetrisBackground.hpp"
#include "TetrisBrick.hpp"
#include "Input.hpp"

void BackToMainMenu( Game* _pGame, void* _data );

TetrisScene::TetrisScene( SceneManager* _pSceneManager, Game* _pGame ) :
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_pSceneManager( _pSceneManager ),
    m_backButton( new Button( m_pGame ) ),
    m_tetrisBackground( new TetrisBackground( m_pGame ) ),
    m_brickFallIntervall( 0.0f )
{
}

TetrisScene::~TetrisScene()
{
    delete m_backButton;
    delete m_tetrisBackground;
}

void TetrisScene::Update( UpdateStats const& _stats )
{
    m_brickFallIntervall += _stats.m_deltaTime;


    sf::Vector2f const mMove = m_pGame->GetInput()->GetMouseSceneMove();

    if (m_pGame->GetInput()->IsButtonPressed(sf::Mouse::Left))
    {
        m_pGame->GetSceneRenderer()->GetCamera()->Move( mMove );
    }
}

void TetrisScene::OnBegin()
{
    puts( "Hello TetrisScene" );

    m_backButton->SetSize( 70.0f, 50.0f );
    m_backButton->SetText( "Back" );
    m_backButton->SetCallbackFn( BackToMainMenu, m_pSceneManager );

    m_pGame->GetSceneRenderer()->AddDrawable( m_tetrisBackground );
    m_pGame->GetGUIRenderer()->AddDrawable( m_backButton );
}

void TetrisScene::OnEnd()
{
    m_pGame->GetSceneRenderer()->ClearAllDrawables();
    m_pGame->GetGUIRenderer()->ClearAllDrawables();
}

void BackToMainMenu( Game* _pGame, void* _data )
{
    SceneManager *const sceneManager = reinterpret_cast<SceneManager*>( _data );

    if( sceneManager ) sceneManager->BeginScene( Scene_IF::SceneID_MainMenu );
}
