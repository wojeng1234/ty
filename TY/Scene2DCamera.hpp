#pragma once

#include <SFML/System/Vector2.hpp>

class Scene2DCamera
{
public:
    Scene2DCamera();
    ~Scene2DCamera();

    inline void SetCenter( sf::Vector2f const& _centerPoint ) { m_centerPoint = _centerPoint; }
    inline void SetViewSize( sf::Vector2f const& _viewSize ) { m_viewSize = _viewSize; }

    inline void Move( sf::Vector2f const& _moveVector ) { SetCenter( m_centerPoint + _moveVector ); }

    inline sf::Vector2f GetCenter() const { return m_centerPoint; }
    inline sf::Vector2f GetViewSize() const { return m_viewSize; }

private:

    sf::Vector2f m_viewSize;
    sf::Vector2f m_centerPoint;
};

