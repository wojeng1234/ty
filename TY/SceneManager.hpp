#pragma once

class Game;
class Scene_IF;

class SceneManager
{
public:
    SceneManager( Game *_pGame );
    ~SceneManager();

    enum
    {
        State_BannerSequence,
        State_MainMenu,
        State_InGame
    };

    bool Init( Game * _pGame );
    void ShutDown();

    void BeginFirstScene();
    void BeginScene( int _sceneID );

private:

    Scene_IF* m_currScene;

    Game *m_pGame;
};

