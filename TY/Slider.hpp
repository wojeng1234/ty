#pragma once

#include "UIWidget.hpp"

class SliderState;

namespace sf
{
    class RectangleShape;
}

class Slider :
    public UIWidget
{
    typedef UIWidget SUPER;

    friend class SliderStateNotHovered;
    friend class SliderStateHovered;
    friend class SliderStatePressed;
    friend class SliderStateReleased;

public:
    Slider( Game *_pGame );
    ~Slider();

    void Update( UpdateStats const &_stats ) override;

    void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;

    void SetRange( float _min, float _max );
    void SetVal( float _val );
    void AddVal( float _delta );

    float GetMin() { return m_minVal; }
    float GetMax() { return m_maxVal; }
    float GetRange() { return m_range; }
    float GetCurrVal() { return m_currVal; }
    float GetValueInPercentage();
    float GetVisualFillToPercentage();

private:

    void OnResized() override;

    SliderState* m_currState;
    void SetCurrState( SliderState* _state );

    sf::RectangleShape *m_background;
    sf::RectangleShape *m_slider;

    float m_minVal;
    float m_maxVal;
    float m_range;
    float m_currVal;
};

