#include "Ty2dDrawable.hpp"

#include <SFML/Graphics.hpp>

Ty2dDrawable::Ty2dDrawable( sf::Drawable* _pDrawable, sf::RenderStates* _pRenderStates ) :
    m_pDrawable( _pDrawable ),
    m_pRenderStates( _pRenderStates )
{
}

Ty2dDrawable::Ty2dDrawable( sf::Drawable* _pDrawable ) :
    m_pDrawable( _pDrawable ),
    m_pRenderStates( new sf::RenderStates() )
{
}

Ty2dDrawable::~Ty2dDrawable()
{
    delete m_pDrawable;
    delete m_pRenderStates;
}
