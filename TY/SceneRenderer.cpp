#include "SceneRenderer.hpp"

#include "Scene2DCamera.hpp"

#include <SFML/Graphics.hpp>

SceneRenderer::SceneRenderer( sf::RenderWindow *_renderWindow ) :
    m_drawables(),
    m_renderWindow( _renderWindow ),
    m_camera( new Scene2DCamera() ),
    m_shader( new sf::Shader() )
{
    m_camera->SetViewSize( sf::Vector2f(1280.0f, 720.0f) );

    std::string const vertShader = "files/shaders/simple_texture_sprite.vert";
    std::string const fragShader = "files/shaders/simple_texture_sprite.frag";

    m_shader->loadFromFile( vertShader, fragShader );
}

SceneRenderer::~SceneRenderer()
{
    delete m_camera;
    delete m_shader;
}

void SceneRenderer::Render() const
{
    sf::Vector2f const viewSize( m_camera->GetViewSize() );
    sf::Vector2f const viewCenter( m_camera->GetCenter() );
    sf::View const sceneView( viewCenter, viewSize );

    sf::RenderStates sceneRenderStates;
    sceneRenderStates.shader = m_shader;
    //m_shader->setUniform( "texture", sf::Shader::CurrentTexture );

    m_renderWindow->setView( sceneView );

    for( sf::Drawable *drawable : m_drawables )
    {
        m_renderWindow->draw( *drawable, sceneRenderStates );
    }
}

void SceneRenderer::AddDrawable( sf::Drawable *_drawable )
{
    m_drawables.push_back( _drawable );
}

void SceneRenderer::RemoveDrawable( sf::Drawable* _drawable )
{
    m_drawables.remove( _drawable );
}

void SceneRenderer::ClearAllDrawables()
{
    m_drawables.clear();
}
