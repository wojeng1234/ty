#include "ButtonState.hpp"
#include "Button.hpp"
#include "Input.hpp"
#include <SFML/Graphics.hpp>

ButtonStateNotHovered ButtonState::NotHovered;
ButtonStateHovered ButtonState::Hovered;
ButtonStateClicked ButtonState::Clicked;
ButtonStateReleased ButtonState::Released;

////////////////////////////////////////////////////
void ButtonStateNotHovered::Update( Button* _button, Input* _input )
{
    if( _button->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ))
    {
        _button->SetCurrState( &ButtonState::Hovered );
    }
}

void ButtonStateNotHovered::OnEnter( Button* _button )
{
    _button->m_rect->setFillColor( sf::Color::Green );
}

////////////////////////////////////////////////////
void ButtonStateHovered::Update( Button* _button, Input* _input )
{
    if( !_button->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ))
    {
        _button->SetCurrState( &ButtonState::NotHovered );
    }
    else if( _input->WasButtonPressed( sf::Mouse::Left ) )
    {
        _button->SetCurrState( &ButtonState::Clicked );
    }
}

void ButtonStateHovered::OnEnter( Button* _button )
{
    _button->m_rect->setFillColor( sf::Color::Red );
}

////////////////////////////////////////////////////
void ButtonStateClicked::Update( Button* _button, Input* _input )
{
    if( !_button->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ))
    {
        _button->SetCurrState( &ButtonState::NotHovered );
    }
    else if( _input->WasButtonReleased( sf::Mouse::Left ) )
    {
        _button->SetCurrState( &ButtonState::Released );
    }
}

void ButtonStateClicked::OnEnter( Button* _button )
{
    _button->m_rect->setFillColor( sf::Color( 128, 0, 0 ) ); // Darker red
}

////////////////////////////////////////////////////
void ButtonStateReleased::Update( Button* _button, Input* _input )
{
    _button->SetCurrState( &ButtonState::Hovered );

    _button->CallCallbackFn();
}
