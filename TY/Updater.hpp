#pragma once

class IUpdateable;

class Updater
{
public:
    Updater();
    ~Updater();

    void Register( IUpdateable *_item );
    void Unregister( IUpdateable *_item );

    void Update( float _deltaTime, float _totalTime );

private:

    int m_numUpdates;
    IUpdateable **m_updateables;

};
