#pragma once

namespace sf
{
    class Drawable;
    class RenderStates;
}

class Ty2dDrawable
{
public:
    Ty2dDrawable( sf::Drawable *_pDrawable, sf::RenderStates *_pRenderStates );
    Ty2dDrawable( sf::Drawable *_pDrawable );
    ~Ty2dDrawable();

private:

    sf::Drawable *m_pDrawable;
    sf::RenderStates *m_pRenderStates;
};
