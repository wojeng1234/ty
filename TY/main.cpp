#include "Game.hpp"
#include "SceneManager.hpp"

int main()
{
    Game* pGame = new Game();

    pGame->Init();
    pGame->Start();

    pGame->DoMainLoop();

    pGame->ShutDown();

    delete pGame;
    return 0;
}
