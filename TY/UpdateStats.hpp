#pragma once

class UpdateStats
{
public:
    UpdateStats( float _deltaTime, float _totalTime ) :
          m_deltaTime( _deltaTime )
        , m_totalTime( _totalTime )
        {}

    float const m_deltaTime;
    float const m_totalTime;
};
