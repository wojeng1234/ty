#include "Game.hpp"
#include "SceneManager.hpp"
#include "SceneRenderer.hpp"
#include "GUIRenderer.hpp"
#include "Updater.hpp"
#include "Input.hpp"

#include <SFML/Graphics.hpp>

Game::Game() :
    m_window( new sf::RenderWindow() ),
    m_sceneManager( new SceneManager(this) ),
    m_sceneRenderer( new SceneRenderer(m_window) ),
    m_GUIRenderer( new GUIRenderer(m_window) ),
    m_pUpdater( new Updater() ),
    m_input( new Input( m_window, m_sceneRenderer->GetCamera() ) ),
    m_clock( new sf::Clock() ),
    m_totalTile( 0.0f ),
    m_deltaTime( 0.0f ),
    m_wndClearColor( new sf::Color( sf::Color::Black ) )
{
}

Game::~Game()
{
}

bool Game::Init()
{
    bool isInitOk = true;

    m_window->create( sf::VideoMode(1024, 600), "TankYou" );

    m_clock->restart();
    m_totalTile = 0.0f;
    m_deltaTime = 0.0f;

    return isInitOk;
}

void Game::Start()
{
    m_sceneManager->BeginFirstScene();
}

void Game::ShutDown()
{
    m_sceneManager->ShutDown();

    delete m_clock;
    delete m_sceneManager;
    delete m_sceneRenderer;
    delete m_GUIRenderer;
    delete m_input;
    delete m_window;
    delete m_wndClearColor;
}

void Game::DoMainLoop()
{
    while( m_window->isOpen() )
    {
        m_deltaTime = m_clock->restart().asSeconds();
        m_totalTile += m_deltaTime;

        sf::Event event;
        while( m_window->pollEvent(event) )
        {
            if ( event.type == sf::Event::Closed )
            {
                CloseWindow();
            }

            m_input->UpdateWithSysEvent( &event );
        }

        m_pUpdater->Update( m_deltaTime, m_totalTile );

        m_window->clear( *m_wndClearColor );

        m_sceneRenderer->Render();
        m_GUIRenderer->Render();

        m_window->display();

        m_input->Update(); // Not the IUpdateable method.

        //60 framer per second
        sf::sleep( sf::seconds(0.016) - sf::seconds(m_deltaTime) );
    }
}

void Game::SetWndClearColor( sf::Color const &_color )
{
    m_wndClearColor->r = _color.r;
    m_wndClearColor->g = _color.g;
    m_wndClearColor->b = _color.b;
    m_wndClearColor->a = _color.a;
}

void Game::ExitGame()
{
    CloseWindow();
}

void Game::CloseWindow()
{
    m_window->close();
}
