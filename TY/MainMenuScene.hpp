#pragma once

#include "Scene_IF.hpp"
#include "IUpdateable.hpp"
#include <string>

class Game;
class SceneManager;
class Button;
class Slider;

namespace sf
{
    class Font;
    class Text;
}

class MainMenuScene :
    public Scene_IF,
    public IUpdateable
{
public:
    MainMenuScene( SceneManager* _pSceneManager, Game* _pGame );
    ~MainMenuScene();

    void Update( UpdateStats const& _stats ) override;

    int GetSceneID() override { return SceneID_BannerSequence; }

    void StartStopCounting();

private:

    void OnBegin() override;
    void OnEnd() override;

    Game* m_pGame;
    SceneManager* m_pSceneManager;

    Button *m_exitGameBtn;
    Button* m_startStopCounterBtn;

    Slider *m_slider;

    sf::Font* m_sliderValueFont;
    sf::Text *m_sliderValue;

    const std::string m_letters;
    int m_counter;
    bool m_isCounterStoped;
};

