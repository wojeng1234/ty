#include "MainMenuScene.hpp"
#include "Updater.hpp"
#include "Game.hpp"
#include "SceneManager.hpp"
#include "GUIRenderer.hpp"
#include "Input.hpp"
#include "Button.hpp"
#include "Slider.hpp"

#include <SFML/Graphics.hpp>

void ExitGameClb( Game *_pGame, void *_data );
void StopCounterClb(Game* _pGame, void* _data);

MainMenuScene::MainMenuScene( SceneManager* _pSceneManager, Game* _pGame ) :
    Scene_IF(),
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_pSceneManager( _pSceneManager ),

    m_exitGameBtn( new Button( m_pGame ) ),
    m_startStopCounterBtn(new Button(m_pGame)),

    m_slider( new Slider( m_pGame ) ),

    m_sliderValueFont( new sf::Font() ),
    m_sliderValue( new sf::Text() ),

    m_letters("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    m_counter(-1), // To start with A
    m_isCounterStoped(false)
{
}

MainMenuScene::~MainMenuScene()
{
    delete m_exitGameBtn;
    delete m_startStopCounterBtn;

    delete m_slider;

    delete m_sliderValueFont;
    delete m_sliderValue;
}

void MainMenuScene::Update( UpdateStats const& _stats )
{
    if (!m_isCounterStoped) {
        m_counter++;
        m_counter = m_counter % m_letters.length();
    }

    m_sliderValue->setString(m_letters[m_counter]);

    //m_sliderValue->setString( std::to_string( m_slider->GetCurrVal() ) );
}

void MainMenuScene::OnBegin()
{
    puts("Hello MainMenuScene!");

    std::string const fontName = "files/fonts/RobotoCondensed-Light.ttf";

    sf::Vector2f const screenSize = sf::Vector2f(m_pGame->GetGUIRenderer()->GetWindowSize());
    sf::Vector2f const btnSize = sf::Vector2f( 300.0f, 60.0f );

    m_sliderValueFont->loadFromFile( fontName );

    m_exitGameBtn->SetSize( btnSize.x, btnSize.y );
    m_exitGameBtn->SetPosition( 10.0f, 10.0f );
    m_exitGameBtn->SetText( "Exit" );
    m_exitGameBtn->SetCallbackFn( ExitGameClb );

    m_startStopCounterBtn->SetSize(btnSize.x, btnSize.y);
    m_startStopCounterBtn->SetPosition(10.0f, 100.0f);
    m_startStopCounterBtn->SetText("Stop counting");
    m_startStopCounterBtn->SetCallbackFn(StopCounterClb, this);

    m_slider->SetSize( btnSize.x, btnSize.y );
    m_slider->SetPosition( 10.0f, 200.0f );
    m_slider->SetRange( 0.0f, 100.0f );

    m_sliderValue->setFont( *m_sliderValueFont );
    m_sliderValue->setString( std::to_string( m_slider->GetCurrVal() ) );
    m_sliderValue->setFillColor( sf::Color::Black );
    m_sliderValue->setPosition( m_slider->GetPosition().x + m_slider->GetSize().x, 100.0f );

    m_pGame->SetWndClearColor( sf::Color::White );

    m_pGame->GetGUIRenderer()->AddDrawable( m_exitGameBtn );
    m_pGame->GetGUIRenderer()->AddDrawable(m_startStopCounterBtn);
    m_pGame->GetGUIRenderer()->AddDrawable( m_slider );
    m_pGame->GetGUIRenderer()->AddDrawable( m_sliderValue );
}

void MainMenuScene::OnEnd()
{
    puts( "Goodbye MainMenuScene!" );

    m_pGame->GetGUIRenderer()->ClearAllDrawables();
}

void MainMenuScene::StartStopCounting()
{
    m_isCounterStoped = !m_isCounterStoped;

    if (m_isCounterStoped) {
        m_startStopCounterBtn->SetText("Start counting");
    }
    else {
        m_startStopCounterBtn->SetText("Stop counting");
    }
}

void ExitGameClb( Game* _pGame, void* _data )
{
    _pGame->ExitGame();
}

void StopCounterClb(Game* _pGame, void* _data)
{
    MainMenuScene* pMainMenuScene = (MainMenuScene*)_data;

    pMainMenuScene->StartStopCounting();
}
