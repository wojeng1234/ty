#include "TetrisBackground.hpp"
#include "TetrisBrick.hpp"
#include "Game.hpp"
#include "Input.hpp"

#include <SFML/Graphics.hpp>

TetrisBackground::TetrisBackground( Game *_pGame ) :
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_width( 10 ),
    m_height( 20 ),
    m_cellSize( 20.0f ),
    m_verticesCount( 2 * (m_width + 1) + 2 * (m_height + 1) ),
    m_vertices( new sf::Vertex[m_verticesCount] ),
    m_cells( new BackgroundCell[m_width * m_height] ),
    m_currFallingBrick( nullptr ),
    m_brickX(0), m_brickY(0),
    m_fallTimeInterval(0.0f), m_fallTime(1.0f)
{
    for (int i = 0; i < 2 * (m_width + 1); i += 2)
    {
        m_vertices[i].position = sf::Vector2f(i/2 * m_cellSize, 0.0f);
        m_vertices[i+1].position = sf::Vector2f(m_vertices[i].position.x, m_height * m_cellSize);
    }

    for (int i = 2 * (m_width + 1); i < m_verticesCount; i += 2)
    {
        int index = i - 2 * (m_width + 1);
        m_vertices[i].position = sf::Vector2f(0.0f, index / 2 * m_cellSize);
        m_vertices[i + 1].position = sf::Vector2f(m_width * m_cellSize, m_vertices[i].position.y);
    }

    for( int i = 0; i < m_verticesCount; ++i )
    {
        m_vertices[i].color = sf::Color::Black;
    }

    GenerateBrick();
}

TetrisBackground::~TetrisBackground()
{
    delete[] m_cells;
    delete[] m_vertices;
}

void TetrisBackground::Update( UpdateStats const& _stats )
{
    int const yMax = (m_brickY + 4) > m_height ? m_height : (m_brickY + 4);
    int const xMax = (m_brickX + 4) > m_width ? m_width : (m_brickX + 4);

#if _DEBUG
    if (m_pGame->GetInput()->IsKeyPressed(sf::Keyboard::Space))
    {
        m_fallTimeInterval = 0.1f;
    }
    else
    {
        m_fallTimeInterval = 1.0f;
    }
#endif

    ProcessInput();

    //Clear background
    for (int y = 0; y < m_height; ++y)
    { 
        for (int x = 0; x < m_width; ++x)
        {
            if( m_cells[y * m_width + x].m_currState != BackgroundCell::state::SOLIDIFIED )
            {
                m_cells[y * m_width + x].m_color = 0;
                m_cells[y * m_width + x].m_currState = BackgroundCell::state::EMPTY;
            }
        }
    }

    //Insert the falling brick into the background
    for (int y = 0; y < 4; ++y)
    {
        for (int x = 0; x < 4; ++x)
        {
            int const color = m_currFallingBrick->GetData()[y * 4 + x];
            if( color )
            {
                m_cells[(y + m_brickY) * m_width + (x + m_brickX)].m_color = color;
                m_cells[(y + m_brickY) * m_width + (x + m_brickX)].m_currState = BackgroundCell::state::FALLING;
            }
        }
    }

    //Fall
    m_fallTime += _stats.m_deltaTime;
    if( m_fallTime > m_fallTimeInterval )
    {
        //Check if falling brick reached something underneath
        for (int y = m_brickY; y < yMax; ++y)
        {
            for (int x = m_brickX; x < xMax; ++x)
            {
                if (m_cells[y * m_width + x].m_currState == BackgroundCell::state::FALLING)
                {
                    if (y == m_height - 1 || m_cells[(y + 1) * m_width + x].m_currState == BackgroundCell::state::SOLIDIFIED)
                    {
                        //Bottom of background reached or there is a solidified one undearneath
                        Solidify();
                        CheckTable();
                        m_brickY = -1;
                        m_brickX = m_width / 2;
                        GenerateBrick();
                    }
                }
            }
        }

        m_brickY++;
        m_fallTime = 0.0f;
    }
}

void TetrisBackground::ProcessInput()
{
    int const yMax = (m_brickY + 4) > m_height ? m_height : (m_brickY + 4);
    int const xMax = (m_brickX + 4) > m_width ? m_width : (m_brickX + 4);

    if (m_pGame->GetInput()->WasKeyPressed(sf::Keyboard::D))
    {
        bool canBeMovedToTheRight = true;
        for (int y = m_brickY; y < yMax; ++y)
        {
            for (int x = m_brickX; x < xMax; ++x)
            {
                if (m_cells[y * m_width + x].m_currState == BackgroundCell::state::FALLING &&
                    (x >= m_width - 1 || m_cells[y * m_width + x + 1].m_currState == BackgroundCell::state::SOLIDIFIED))
                {
                    canBeMovedToTheRight = false;
                }
            }
        }

        if (canBeMovedToTheRight) m_brickX++;
    }
    else if (m_pGame->GetInput()->WasKeyPressed(sf::Keyboard::A))
    {
        bool canBeMovedToTheLeft = true;
        for (int y = m_brickY; y < yMax; ++y)
        {
            for (int x = m_brickX; x < xMax; ++x)
            {
                if (m_cells[y * m_width + x].m_currState == BackgroundCell::state::FALLING &&
                    (x <= 0 || m_cells[y * m_width + x - 1].m_currState == BackgroundCell::state::SOLIDIFIED))
                {
                    canBeMovedToTheLeft = false;
                }
            }
        }

        if (canBeMovedToTheLeft) m_brickX--;
    }
    else if (m_pGame->GetInput()->WasKeyPressed(sf::Keyboard::R))
    {
        bool canBeRotated = true;
        TetrisBrick::rotation destRot =
            (TetrisBrick::rotation)((m_currFallingBrick->GetCurrRotation() + 1) % TetrisBrick::rotation::NUM_ROTS);

        for (int y = m_brickY; y < yMax; ++y)
        {
            for (int x = m_brickX; x < xMax; ++x)
            {
                int const brickLocalX = x - m_brickX;
                int const brickLocalY = y - m_brickY;
                if( m_currFallingBrick->GetRotData(destRot)[brickLocalY * 4 + brickLocalX] &&
                    m_cells[y * m_width + x].m_currState == BackgroundCell::state::SOLIDIFIED )
                {
                    canBeRotated = false;
                }
            }
        }

        if( canBeRotated )
        {
            for (int y = 0; y < 4; ++y)
            {
                for (int x = 0; x < 4; ++x)
                {
                    if( m_currFallingBrick->GetRotData(destRot)[y * 4 + x] &&
                        x + m_brickX >= m_width )
                    {
                        canBeRotated = false;
                    }
                }
            }
        }

        if (canBeRotated) m_currFallingBrick->Rotate(destRot);
    }
}

void TetrisBackground::GenerateBrick()
{
    TetrisBrick* bricksArray[6] = {
        &TetrisBrick::BrickS,
        &TetrisBrick::BrickZ,
        &TetrisBrick::BrickL,
        &TetrisBrick::BrickJ,
        &TetrisBrick::BrickT,
        &TetrisBrick::BrickI
    };

    m_currFallingBrick = bricksArray[rand() % 6];
    //m_currFallingBrick = &TetrisBrick::BrickI;

    int col = (rand() % 7) + 1;
    m_currFallingBrick->SetColor( col );
}

void TetrisBackground::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    sf::RectangleShape rect;
    rect.setSize( sf::Vector2f(m_cellSize, m_cellSize) );

    for (int y = 0; y < m_height; ++y)
    {
        for (int x = 0; x < m_width; ++x)
        {
            if( m_cells[y * m_width + x].m_currState )
            {
                rect.setFillColor( GetColorByIndex(m_cells[y * m_width + x].m_color) );
                rect.setPosition( sf::Vector2f( x * m_cellSize, y * m_cellSize ) );
                target.draw( rect, states );
            }
        }
    }

    target.draw( m_vertices, m_verticesCount, sf::PrimitiveType::Lines, states );
}

sf::Color TetrisBackground::GetColorByIndex( int _index ) const
{
    switch ( _index )
    {
    case 1:
        return sf::Color::Black;
    case 2:
        return sf::Color::Red;
    case 3:
        return sf::Color::Green;
    case 4:
        return sf::Color::Blue;
    case 5:
        return sf::Color::Yellow;
    case 6:
        return sf::Color::Magenta;
    case 7:
        return sf::Color::Cyan;
    default:
          return sf::Color::White;
    }
}

void TetrisBackground::Solidify()
{
    int const yMax = (m_brickY + 5) > m_height ? m_height : (m_brickY + 5);
    int const xMax = (m_brickX + 4) > m_width ? m_width : (m_brickX + 4);
    for (int y = m_brickY; y < yMax; ++y)
    {
        for (int x = m_brickX; x < xMax; ++x)
        {
            if( m_cells[y * m_width + x].m_currState == BackgroundCell::state::FALLING )
            {
                m_cells[y * m_width + x].m_currState = BackgroundCell::state::SOLIDIFIED;
            }
        }
    }
}

void TetrisBackground::CheckTable()
{
    int bricksInRowCount = 0;

    for (int y = 0; y < m_height; ++y)
    {
        for (int x = 0; x < m_width; ++x)
        {
            if (m_cells[y * m_width + x].m_currState == BackgroundCell::state::SOLIDIFIED)
            {
                bricksInRowCount++;
            }
        }

        if (bricksInRowCount == m_width)
        {
            //This row is full
            for (int r = y; r > 0; --r)
            {
                for (int c = 0; c < m_width; ++c)
                {
                    m_cells[r * m_width + c] = m_cells[(r - 1) * m_width + c];
                    m_cells[(r - 1) * m_width + c] = BackgroundCell();
                }
            }
        }

        bricksInRowCount = 0;
    }
}

TetrisBackground::BackgroundCell::BackgroundCell() :
    m_currState( state::EMPTY ),
    m_color( 0 )
{
}
