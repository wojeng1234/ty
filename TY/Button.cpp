#include "Button.hpp"

#include "Game.hpp"
#include "Input.hpp"
#include "Updater.hpp"
#include "ButtonState.hpp"
#include <SFML/Graphics.hpp>

Button::Button( Game *_pGame ) :
    SUPER( _pGame ),
    m_rect( new sf::RectangleShape() ),
    m_font( new sf::Font() ),
    m_text( new sf::Text() ),
    m_currState( nullptr ),
    m_pData( nullptr ),
    m_pCallback( nullptr )
{
    SetCurrState( &ButtonState::NotHovered );
}

Button::~Button()
{
    delete m_rect;
    delete m_text;
    delete m_font;
}

void Button::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    target.draw( *m_rect );
    target.draw( *m_text );
}

void Button::Update( UpdateStats const& _stats )
{
    m_currState->Update( this, m_pGame->GetInput() );
}

void Button::SetText( std::string const& _text )
{
    std::string const fontName = "files/fonts/RobotoCondensed-Light.ttf";
    m_font->loadFromFile( fontName );
    m_text->setFont( *m_font );
    m_text->setFillColor( sf::Color::Black );
    m_text->setString( _text );

    sf::FloatRect const textRect = m_text->getLocalBounds();
    sf::FloatRect const btnLocalRect = sf::FloatRect( 0.0f, 0.0f, GetSize().x, GetSize().y );
    sf::Vector2f const btnPos = GetPosition();

    float const textXPos = ((btnLocalRect.left + btnLocalRect.width) * 0.5f - textRect.width * 0.5f);
    float const textYPos = ((btnLocalRect.top + btnLocalRect.height) * 0.5f - textRect.height * 0.5f);
    m_text->setPosition( sf::Vector2f( textXPos, textYPos ) + btnPos );
}

void Button::SetCallbackFn( btnCallbackFn _callback, void* _pData )
{
    m_pCallback = _callback;
    m_pData = _pData;
}

void Button::OnResized()
{
    m_rect->setPosition( GetPosition() );
    m_rect->setSize( GetSize() );
}

void Button::SetCurrState( ButtonState* _state )
{
    if( m_currState ) m_currState->OnLeave( this );

    m_currState = _state;
    m_currState->OnEnter( this );
}

void Button::CallCallbackFn()
{
    if( m_pCallback )
    {
        m_pCallback( m_pGame, m_pData );
    }
}
