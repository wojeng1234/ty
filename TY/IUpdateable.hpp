#pragma once

#include "UpdateStats.hpp"

class Updater;

class IUpdateable
{
public:
    IUpdateable( Updater *_updater );
    virtual ~IUpdateable();

    virtual void Update( UpdateStats const &_stats ) = 0;

private:

    Updater *m_pUpdater;
};
