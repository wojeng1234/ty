#pragma once

#include "Scene_IF.hpp"
#include "IUpdateable.hpp"

class Game;
class SceneManager;

namespace sf
{
    class RectangleShape;
    class Texture;
}

class GameplayScene :
    public Scene_IF,
    public IUpdateable
{
public:
    GameplayScene( SceneManager* _pSceneManager, Game* _pGame );
    ~GameplayScene();

    void Update( UpdateStats const& _stats ) override;

    int GetSceneID() override { return SceneID_Gameplay; }

private:

    void OnBegin() override;
    void OnEnd() override;

    Game* m_pGame;
    SceneManager* m_pSceneManager;

    sf::RectangleShape *m_rect;
    sf::Texture *m_texture;
};
