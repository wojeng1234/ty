#include "IUpdateable.hpp"
#include "Updater.hpp"

IUpdateable::IUpdateable( Updater *_updater ) :
    m_pUpdater( _updater )
{
    m_pUpdater->Register( this );
}

IUpdateable::~IUpdateable()
{
    m_pUpdater->Unregister( this );
}
