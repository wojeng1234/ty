#pragma once

class Button;
class Input;

class ButtonStateNotHovered;
class ButtonStateHovered;
class ButtonStateClicked;
class ButtonStateReleased;

class ButtonState
{
public:
    ButtonState() {}
    virtual ~ButtonState() {}

    virtual void Update( Button *_button, Input *_input ) = 0;
    virtual void OnEnter( Button* _button ) {};
    virtual void OnLeave( Button* _button ) {};

    static ButtonStateNotHovered NotHovered;
    static ButtonStateHovered Hovered;
    static ButtonStateClicked Clicked;
    static ButtonStateReleased Released;
};

////////////////////////////////////////////////////
class ButtonStateNotHovered :
    public ButtonState
{
public:
    void Update( Button *_button, Input* _input ) override;
    void OnEnter( Button* _button ) override;
};

////////////////////////////////////////////////////
class ButtonStateHovered :
    public ButtonState
{
public:
    void Update( Button* _button, Input* _input ) override;
    void OnEnter( Button* _button ) override;
};

////////////////////////////////////////////////////
class ButtonStateClicked :
    public ButtonState
{
public:
    void Update( Button* _button, Input* _input ) override;
    void OnEnter( Button* _button ) override;
};

////////////////////////////////////////////////////
class ButtonStateReleased :
    public ButtonState
{
public:
    void Update( Button* _button, Input* _input ) override;
};
