#include "BannerSequenceScene.hpp"
#include "Updater.hpp"
#include "Game.hpp"
#include "SceneManager.hpp"
#include "SceneRenderer.hpp"
#include "Input.hpp"

#include <SFML/Graphics.hpp>

BannerSequenceScene::BannerSequenceScene( SceneManager* _pSceneManager, Game* _pGame ) :
    Scene_IF(),
    IUpdateable( _pGame->GetUpdater() ),
    m_pGame( _pGame ),
    m_pSceneManager( _pSceneManager ),
    m_totalDisplayTime( 10.0f ),
    m_timeElapsed( 0.0f ),

    m_texture( new sf::Texture() ),
    m_sprite( new sf::RectangleShape() ),

    m_bannerLabelFont( new sf::Font() ),
    m_label1( new sf::Text() ),
    m_label2( new sf::Text() )
{
}

BannerSequenceScene::~BannerSequenceScene()
{
    delete m_sprite;
    delete m_texture;

    delete m_label1;
    delete m_label2;
    delete m_bannerLabelFont;
}

void BannerSequenceScene::Update( UpdateStats const& _stats )
{
    m_timeElapsed += _stats.m_deltaTime;

    sf::Uint8 const alfa = (sf::Uint8)255.0f * (m_timeElapsed / m_totalDisplayTime);

    m_sprite->setFillColor( sf::Color( 255, 255, 255, alfa ) );

    if (m_pGame->GetInput()->WasKeyPressed(sf::Keyboard::Escape) || m_timeElapsed > m_totalDisplayTime )
    {
        m_pSceneManager->BeginScene( Scene_IF::SceneID_MainMenu );
    }
}

void BannerSequenceScene::OnBegin()
{
    puts("Hello BannerSequenceScene!");

    m_timeElapsed = 0.0f;

    std::string const bannerName = "files/pictures/WojtekAndWojciech.png";
    //std::string const bannerName = "Sonicom-logo.png";
    std::string const bannerLabel1 = "Wojtek & Wojciech";
    std::string const bannerLabel2 = "Entertaiment";
    std::string const fontName = "files/fonts/RobotoCondensed-Light.ttf";

    //Image
    m_texture->loadFromFile( bannerName );
    sf::Vector2f const bannerSize = sf::Vector2f( 300.0f, 300.0f );

    m_sprite->setTexture( m_texture );
    m_sprite->setSize( bannerSize );
    m_sprite->setOrigin( bannerSize * 0.5f );
    //m_sprite->setScale( sf::Vector2f( 300.0f / (float)texSize.x, 300.0f / (float)texSize.y) );

    //Labels
    m_bannerLabelFont->loadFromFile( fontName );

    m_label1->setFont( *m_bannerLabelFont );
    m_label1->setString( bannerLabel1 );
    m_label1->setFillColor( sf::Color::Black );
    m_label1->setPosition( sf::Vector2f(-m_label1->getLocalBounds().width * 0.5f, -190.0f) );

    m_label2->setFont( *m_bannerLabelFont );
    m_label2->setString( bannerLabel2 );
    m_label2->setFillColor( sf::Color::Black );
    m_label2->setPosition( sf::Vector2f(-m_label2->getLocalBounds().width * 0.5f, 150.0f) );

    m_pGame->GetSceneRenderer()->AddDrawable( m_sprite );
    m_pGame->GetSceneRenderer()->AddDrawable( m_label1 );
    m_pGame->GetSceneRenderer()->AddDrawable( m_label2 );

    m_pGame->SetWndClearColor( sf::Color::White );
}

void BannerSequenceScene::OnEnd()
{
    m_pGame->GetSceneRenderer()->ClearAllDrawables();
}
