#include "TetrisBricksCollection.hpp"

#include "TetrisBrick.hpp"

TetrisBricksCollection::TetrisBricksCollection() :
    m_numTypes( TetrisBrick::type::NUM_TYPES ),
    m_numRots( TetrisBrick::rotation::NUM_ROTS ),
    m_bricksCollection( nullptr )
{
    m_bricksCollection = new int** [m_numTypes];
    for( int t = 0; t < m_numTypes; ++t )
    {
        m_bricksCollection[t] = new int* [m_numRots];
    }

    Set_S();
    Set_Z();
    Set_L();
    Set_J();
    Set_I();
    Set_T();
}

TetrisBricksCollection::~TetrisBricksCollection()
{
    for( int t = 0; t < m_numTypes; ++t )
    {
        for( int r = 0; r < m_numRots; ++r )
        {
            delete[] m_bricksCollection[t][r];
        }
        delete[] m_bricksCollection[t];
    }
    delete[] m_bricksCollection;
}

int *TetrisBricksCollection::GetBrickData( int _type, int _rot )
{
    return m_bricksCollection[_type][_rot];
}

void TetrisBricksCollection::ZeroBrickData( int* _data )
{
    for( int r = 0; r < 4; ++r )
    { 
        for( int c = 0; c < 4; ++c )
        {
            _data[(r * 4) + c] = 0;
        }
    }
}

void TetrisBricksCollection::Set_S()
{
    m_bricksCollection[TetrisBrick::type::S][TetrisBrick::rotation::LEFT] = new int[16]{
        0, 1, 1, 0,
        1, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::S][TetrisBrick::rotation::UP] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::S][TetrisBrick::rotation::RIGHT] = new int[16]{
        0, 1, 1, 0,
        1, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::S][TetrisBrick::rotation::DOWN] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };
}

void TetrisBricksCollection::Set_Z()
{
    m_bricksCollection[TetrisBrick::type::Z][TetrisBrick::rotation::LEFT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::Z][TetrisBrick::rotation::UP] = new int[16]{
        0, 1, 0, 0,
        1, 1, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::Z][TetrisBrick::rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::Z][TetrisBrick::rotation::DOWN] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };
}

void TetrisBricksCollection::Set_L()
{
    m_bricksCollection[TetrisBrick::type::L][TetrisBrick::rotation::LEFT] = new int[16]{
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::L][TetrisBrick::rotation::UP] = new int[16]{
        1, 1, 1, 0,
        1, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::L][TetrisBrick::rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::L][TetrisBrick::rotation::DOWN] = new int[16]{
        0, 0, 1, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
}

void TetrisBricksCollection::Set_J()
{
    m_bricksCollection[TetrisBrick::type::J][TetrisBrick::rotation::LEFT] = new int[16]{
        0, 1, 0, 0,
        0, 1, 0, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::J][TetrisBrick::rotation::UP] = new int[16]{
        1, 0, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::J][TetrisBrick::rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::J][TetrisBrick::rotation::DOWN] = new int[16]{
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    };
}

void TetrisBricksCollection::Set_I()
{
    m_bricksCollection[TetrisBrick::type::I][TetrisBrick::rotation::LEFT] = new int[16]{
        0, 0, 0, 0,
        1, 1, 1, 1,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::I][TetrisBrick::rotation::UP] = new int[16]{
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::I][TetrisBrick::rotation::RIGHT] = new int[16]{
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::I][TetrisBrick::rotation::DOWN] = new int[16]{
        0, 0, 0, 0,
        1, 1, 1, 1,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
}

void TetrisBricksCollection::Set_T()
{
    m_bricksCollection[TetrisBrick::type::T][TetrisBrick::rotation::LEFT] = new int[16]{
        1, 1, 1, 0,
        0, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::T][TetrisBrick::rotation::UP] = new int[16]{
        0, 1, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::T][TetrisBrick::rotation::RIGHT] = new int[16]{
        0, 1, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_bricksCollection[TetrisBrick::type::T][TetrisBrick::rotation::DOWN] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };
}
