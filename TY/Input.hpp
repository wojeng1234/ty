#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

class Scene2DCamera;

namespace sf
{
    class RenderWindow;
    class Event;
}

class Input
{
public:
    Input( sf::RenderWindow *_pWindow, Scene2DCamera *_pSceneCamera );
    ~Input();

    enum class input_device_type
    {
        gamepad,
        keyboard,

        none
    };

    //Keyboard
    inline void SetKeyPressed( sf::Keyboard::Key const _key )
    { 
        m_keysPressed[_key] = m_wasKeyReleased[_key];
        m_wasKeyReleased[_key] = false;
    }
    inline void SetKeyReleased( sf::Keyboard::Key const _key )
    {
        m_keysReleased[_key] = true;
        m_wasKeyReleased[_key] = m_keysReleased[_key];
    }

    inline bool WasKeyPressed( sf::Keyboard::Key const _key ) const { return m_keysPressed[_key]; }
    inline bool WasKeyReleased( sf::Keyboard::Key const _key ) const { return m_keysReleased[_key]; }
    inline bool IsKeyPressed( sf::Keyboard::Key const _key ) const { return sf::Keyboard::isKeyPressed(_key); }

    //Mouse
    sf::Vector2i GetMouseWindowPos() const;
    sf::Vector2i GetMouseWindowMove() const;

    sf::Vector2f GetMouseScenePos() const;
    sf::Vector2f GetMouseSceneMove() const;

    inline void SetButtonPressed( sf::Mouse::Button const _btn ) { m_mouseButtonsPressed[_btn] = true; }
    inline void SetButtonReleased( sf::Mouse::Button const _btn ) { m_mouseButtonsReleased[_btn] = true; }

    inline bool WasButtonPressed (sf::Mouse::Button const _btn) const { return m_mouseButtonsPressed[_btn]; }
    inline bool WasButtonReleased (sf::Mouse::Button const _btn) const { return m_mouseButtonsReleased[_btn]; }
    inline bool IsButtonPressed( sf::Mouse::Button const _btn ) const { return sf::Mouse::isButtonPressed(_btn); }

    void UpdateWithSysEvent( sf::Event *_pEvent );
    void Update();

    inline input_device_type GetLastUsedDeviceType() { return m_lastUsedDeviceType; }

private:

    Input::input_device_type m_lastUsedDeviceType;

    sf::RenderWindow* m_pWindow;

    Scene2DCamera *m_pSceneCamera;

    sf::Vector2i m_mouseWindowPos;
    sf::Vector2i m_mouseWindowMove;

    sf::Vector2f m_mouseScenePos;
    sf::Vector2f m_mouseSceneMove;

    bool m_keysPressed[ sf::Keyboard::KeyCount ];
    bool m_keysReleased[ sf::Keyboard::KeyCount ];

    bool m_wasKeyReleased[ sf::Keyboard::KeyCount ];

    bool m_mouseButtonsPressed[ sf::Mouse::ButtonCount ];
    bool m_mouseButtonsReleased[ sf::Mouse::ButtonCount ];
};

