#pragma once

class TetrisSBrick;
class TetrisZBrick;
class TetrisLBrick;
class TetrisJBrick;
class TetrisIBrick;
class TetrisTBrick;

class TetrisBrick
{
public:
    TetrisBrick();
    virtual ~TetrisBrick();

    enum type
    {
        S, Z, L, J, I, T,
        NUM_TYPES
    };

    enum rotation
    {
        LEFT, UP, RIGHT, DOWN,
        NUM_ROTS
    };

    int *GetData() { return m_data[m_currRot]; };
    
    int *GetRotData( rotation _rot ) { return m_data[_rot]; }

    rotation GetCurrRotation() const { return m_currRot; }
    void Rotate( rotation _rot ) { m_currRot = _rot; }

    void SetColor( int _color );

protected:

    int **m_data;
    rotation m_currRot;

public:
    static TetrisSBrick BrickS;
    static TetrisZBrick BrickZ;
    static TetrisLBrick BrickL;
    static TetrisJBrick BrickJ;
    static TetrisIBrick BrickI;
    static TetrisTBrick BrickT;

};

class TetrisSBrick :
    public TetrisBrick
{
public:
    TetrisSBrick();
};

class TetrisZBrick :
    public TetrisBrick
{
public:
    TetrisZBrick();
};

class TetrisLBrick :
    public TetrisBrick
{
public:
    TetrisLBrick();
};

class TetrisJBrick :
    public TetrisBrick
{
public:
    TetrisJBrick();
};

class TetrisIBrick :
    public TetrisBrick
{
public:
    TetrisIBrick();
};

class TetrisTBrick :
    public TetrisBrick
{
public:
    TetrisTBrick();
};
