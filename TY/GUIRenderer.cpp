#include "GUIRenderer.hpp"

#include <SFML/Graphics.hpp>

GUIRenderer::GUIRenderer(sf::RenderWindow* _renderWindow) :
    m_drawables(),
    m_renderWindow(_renderWindow)
{
}

GUIRenderer::~GUIRenderer()
{
}

void GUIRenderer::Render() const
{
    sf::Vector2f const viewSize( m_renderWindow->getSize() );
    sf::Vector2f const viewCenter( viewSize * 0.5f );
    sf::View const sceneView( viewCenter, viewSize );

    m_renderWindow->setView( sceneView );

    for ( sf::Drawable* drawable : m_drawables )
    {
        m_renderWindow->draw(*drawable);
    }
}

void GUIRenderer::AddDrawable( sf::Drawable* _drawable )
{
    m_drawables.push_back( _drawable );
}

void GUIRenderer::RemoveDrawable( sf::Drawable* _drawable )
{
    m_drawables.remove(_drawable);
}

void GUIRenderer::ClearAllDrawables()
{
    m_drawables.clear();
}

sf::Vector2u GUIRenderer::GetWindowSize()
{
    return m_renderWindow->getSize();
}
