#pragma once

#include "IUpdateable.hpp"

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Color.hpp>

class TetrisBrick;
class Game;

namespace sf
{
    class Vertex;
}

class TetrisBackground :
    public sf::Drawable,
    public IUpdateable
{
public:
    TetrisBackground( Game *_pGame );
    ~TetrisBackground();

    void draw( sf::RenderTarget& target, sf::RenderStates states ) const override;

    void Update( UpdateStats const &_stats ) override;

private:

    void ProcessInput();
    void GenerateBrick();

    Game* m_pGame;

    int const m_width;
    int const m_height;

    class BackgroundCell
    {
    public:
        BackgroundCell();

        enum state { EMPTY, SOLIDIFIED, FALLING };

        state m_currState;
        int m_color;
    };

    BackgroundCell *m_cells;

    float const m_cellSize;
    int const m_verticesCount;
    sf::Vertex *m_vertices;

    TetrisBrick *m_currFallingBrick;
    int m_brickX;
    int m_brickY;
    float m_fallTime;
    float m_fallTimeInterval;

    sf::Color GetColorByIndex( int _index ) const;
    void Solidify();
    void CheckTable();
};

