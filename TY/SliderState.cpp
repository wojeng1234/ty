#include "SliderState.hpp"
#include "Slider.hpp"
#include "Input.hpp"

#include <SFML/Graphics.hpp>

SliderStateNotHovered SliderState::NotHovered;
SliderStateHovered SliderState::Hovered;
SliderStatePressed SliderState::Pressed;
SliderStateReleased SliderState::Released;

////////////////////////////////////////////////////
void SliderStateNotHovered::Update( Slider* _slider, Input* _input )
{
    if( _slider->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ) )
    {
        _slider->SetCurrState( &SliderState::Hovered );
    }
}

void SliderStateNotHovered::OnEnter( Slider* _slider, Input* _input )
{
    _slider->m_slider->setFillColor( sf::Color( 0, 128, 0 ) );
    _slider->m_background->setFillColor( sf::Color::Green );
}

////////////////////////////////////////////////////
void SliderStateHovered::Update( Slider* _slider, Input* _input )
{
    if( !_slider->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ) )
    {
        _slider->SetCurrState( &SliderState::NotHovered );
    }
    else if( _input->WasButtonPressed( sf::Mouse::Left ) )
    {
        _slider->SetCurrState( &SliderState::Pressed );
    }
}

void SliderStateHovered::OnEnter( Slider* _slider, Input* _input )
{
    _slider->m_background->setFillColor( sf::Color::Red );
}

////////////////////////////////////////////////////
void SliderStatePressed::Update( Slider* _slider, Input* _input )
{
    if( _input->WasButtonReleased( sf::Mouse::Left ) )
    {
        _slider->SetCurrState( &SliderState::Released );
    }
    else
    {
        float const mouseXPos = _input->GetMouseWindowPos().x;
        float const sliderLeftBorder = _slider->GetPosition().x;
        float const sliderRightBorder = _slider->GetPosition().x + _slider->GetSize().x;
        if( mouseXPos > sliderLeftBorder && mouseXPos < sliderRightBorder )
        {
            float const sliderHeight = _slider->m_slider->getSize().y;
            float const xOffset = _slider->GetPosition().x;
            _slider->m_slider->setSize( sf::Vector2f( mouseXPos - xOffset, sliderHeight ) );
        }
        else if( mouseXPos < sliderLeftBorder )
        {
            float const sliderHeight = _slider->m_slider->getSize().y;
            float const xOffset = 0.0f;
            _slider->m_slider->setSize( sf::Vector2f( xOffset, sliderHeight ) );
        }
        else if( mouseXPos > sliderRightBorder )
        {
            float const sliderHeight = _slider->m_slider->getSize().y;
            float const xOffset = _slider->GetSize().x;
            _slider->m_slider->setSize( sf::Vector2f( xOffset, sliderHeight ) );
        }
    }
}

void SliderStatePressed::OnEnter( Slider* _slider, Input* _input )
{
    _slider->m_slider->setFillColor( sf::Color(128, 0, 0) );
}

////////////////////////////////////////////////////
void SliderStateReleased::Update( Slider* _slider, Input* _input )
{
    if( _slider->GetRect().contains( sf::Vector2f(_input->GetMouseWindowPos()) ) )
    {
        _slider->SetCurrState( &SliderState::Hovered );
    }
    else
    {
        _slider->SetCurrState( &SliderState::NotHovered );
    }
}
