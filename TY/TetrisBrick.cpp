#include "TetrisBrick.hpp"

TetrisSBrick TetrisBrick::BrickS;
TetrisZBrick TetrisBrick::BrickZ;
TetrisLBrick TetrisBrick::BrickL;
TetrisJBrick TetrisBrick::BrickJ;
TetrisIBrick TetrisBrick::BrickI;
TetrisTBrick TetrisBrick::BrickT;

TetrisBrick::TetrisBrick() :
    m_data( new int*[rotation::NUM_ROTS] ),
    m_currRot( rotation::LEFT )
{
}

TetrisBrick::~TetrisBrick()
{
    for( int i = 0; i < rotation::NUM_ROTS; ++i )
    {
        delete[] m_data[i];
    }
    delete[] m_data;
}

void TetrisBrick::SetColor(int _color)
{
    for( int c = 0; c < rotation::NUM_ROTS; ++c )
    {
        for( int i = 0; i < 16; ++i )
        {
            if( m_data[c][i] ) m_data[c][i] = _color;
        }
    }
}

TetrisSBrick::TetrisSBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        0, 1, 1, 0,
        1, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        0, 1, 1, 0,
        1, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };
}

TetrisZBrick::TetrisZBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        0, 1, 0, 0,
        1, 1, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        0, 1, 0, 0,
        1, 1, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };
}

TetrisLBrick::TetrisLBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        1, 1, 1, 0,
        1, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        0, 0, 1, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
}

TetrisJBrick::TetrisJBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        0, 1, 0, 0,
        0, 1, 0, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        1, 0, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        1, 1, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        1, 1, 1, 0,
        0, 0, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
}

TetrisIBrick::TetrisIBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        1, 1, 1, 1,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0,
        1, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        1, 1, 1, 1,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
}

TetrisTBrick::TetrisTBrick()
{
    m_data[rotation::LEFT] = new int[16]{
        0, 0, 0, 0,
        1, 1, 1, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::UP] = new int[16]{
        0, 1, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::RIGHT] = new int[16]{
        0, 1, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    m_data[rotation::DOWN] = new int[16]{
        0, 1, 0, 0,
        0, 1, 1, 0,
        0, 1, 0, 0,
        0, 0, 0, 0
    };
}
