#pragma once

class TetrisBricksCollection
{
public:
    TetrisBricksCollection();
    ~TetrisBricksCollection();

    int *GetBrickData( int _type, int _rot );

private:

    int const m_numTypes;
    int const m_numRots;
    int ***m_bricksCollection;

    void ZeroBrickData( int *_data );

    void Set_S();
    void Set_Z();
    void Set_L();
    void Set_J();
    void Set_I();
    void Set_T();
};