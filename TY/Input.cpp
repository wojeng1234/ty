#include "Input.hpp"

#include "Scene2DCamera.hpp"

#include <SFML/Graphics.hpp>

Input::Input( sf::RenderWindow* _pWindow, Scene2DCamera *_pSceneCamera ) :
    m_pWindow( _pWindow ),
    m_pSceneCamera( _pSceneCamera ),
    m_mouseWindowPos( sf::Vector2i(0, 0) ),
    m_mouseScenePos( sf::Vector2f(0.0f, 0.0f) ),
    m_lastUsedDeviceType( input_device_type::keyboard )
{
    for( int i = 0; i < sf::Keyboard::KeyCount; ++i )
    {
        m_keysPressed[i] = false;
        m_keysReleased[i] = false;
        m_wasKeyReleased[i] = true;
    }

    for( int i = 0; i < sf::Mouse::ButtonCount; ++i )
    {
        m_mouseButtonsPressed[i] = false;
        m_mouseButtonsReleased[i] = false;
    }
}

Input::~Input()
{
}

sf::Vector2i Input::GetMouseWindowPos() const
{
    return m_mouseWindowPos;
}

sf::Vector2i Input::GetMouseWindowMove() const
{
    return m_mouseWindowMove;
}

sf::Vector2f Input::GetMouseScenePos() const
{
    return m_mouseScenePos;
}

sf::Vector2f Input::GetMouseSceneMove() const
{
    return m_mouseSceneMove;
}

void Input::UpdateWithSysEvent( sf::Event* _pEvent )
{
    switch ( _pEvent->type )
    {
    case sf::Event::KeyPressed:
        SetKeyPressed( _pEvent->key.code );
        break;
    case sf::Event::KeyReleased:
        SetKeyReleased( _pEvent->key.code );
        break;
    case sf::Event::MouseButtonPressed:
        SetButtonPressed( _pEvent->mouseButton.button );
        break;
    case sf::Event::MouseButtonReleased:
        SetButtonReleased( _pEvent->mouseButton.button );
        break;
    }
}

void Input::Update()
{
    m_mouseWindowMove = m_mouseWindowPos;
    m_mouseWindowPos = sf::Mouse::getPosition( *m_pWindow );
    m_mouseWindowMove -= m_mouseWindowPos;

    sf::Vector2f const windowSize = sf::Vector2f(m_pWindow->getSize());
    float const vRatio = m_pSceneCamera->GetViewSize().x / windowSize.x;
    float const hRatio = m_pSceneCamera->GetViewSize().y / windowSize.y;

    sf::Vector2f const ratioMouseWindowPos =
        sf::Vector2f(m_mouseWindowPos.x * vRatio, m_mouseWindowPos.y * hRatio);
    sf::Vector2f const zeroPointInWindowView =
        (m_pSceneCamera->GetViewSize() * 0.5f) - m_pSceneCamera->GetCenter();

    m_mouseScenePos = ratioMouseWindowPos - zeroPointInWindowView;
    m_mouseSceneMove = sf::Vector2f(m_mouseWindowMove.x * vRatio, m_mouseWindowMove.y * hRatio);

    for (int i = 0; i < sf::Keyboard::KeyCount; ++i)
    {
        m_keysPressed[i] = false;
        m_keysReleased[i] = false;
    }

    for (int i = 0; i < sf::Mouse::ButtonCount; ++i)
    {
        m_mouseButtonsPressed[i] = false;
        m_mouseButtonsReleased[i] = false;
    }
}
